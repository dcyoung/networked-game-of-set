package lab5;

import java.io.EOFException;
import java.io.IOException;
import java.util.Random;
import java.util.Set;

import lab5.model.Player;
import lab5.model.SetGame;
import lab5.view.GameController;
import lab5.view.Logger;
import lab5.view.NetworkView;

/**
 * Server Execution.... this class instantiates a server (NetworkView) and nothing else really. 
 * @author David (dcyoung@wustl.edu)
 *       File: ServerMain.java
 *	TO test game, run the ServerMain.java from the lab5 package... 
 *	then run a Client.java from the lab5.client package, read its instructions 
 *	and click through its menu.  Be sure to get through the main menu before opening a second client. 
 */
public class ServerMain implements Runnable {

	final private SetGame   model;
	final private Random    rand;
	public static final int DELAY = 1000; // nominal sleep delay (in ms)

	public ServerMain() {
		rand = new Random();
		model = new SetGame();
	}
	
	/**
	 * Unused Run method left over from dummyServer Build
	 */
	public void run() {
	}

	/**
	 * The sleep that is observed nominally.
	 * Change DELAY to speed things up or slow things down.
	 */
	public static void sleep() {
		sleep(DELAY);
	}
	
	/**
	 * Delay for an arbitrary amount of time.
	 * @param ms delay amount (in ms)
	 */
	public static void sleep(int ms) {
		try {
			Thread.sleep(ms);
		}
		catch (Exception e) {
			throw new Error("ServerMain.sleep: should not happen " + e);
		}
	}
	
	/**
	 * Accessor for SetGame model
	 * @return SetGame model
	 */
	public SetGame getSetGame() {
		return model;
	}

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {

		ServerMain mServ = new ServerMain();
		Runnable v = new NetworkView(mServ.getSetGame());
		Thread t = new Thread(v);
		t.start();
	}

}
