package lab5.view;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import lab5.model.SetGame;
import lab5.view.PlayerHandler;
import lab5.view.GameController;

/**
 * The Server of the game, handles initial connections and assigns 
 * PlayerHandlers to Clients.
 * @author David Young (dcyoung@go.wustl.edu)
 * 		 File: NetworkView.java
 */
public class NetworkView implements Runnable {
	SetGame model;
	GameController gameController;
	int port;
	/**
	 * Constructor
	 * @param model
	 * @throws IOException
	 */
	public NetworkView(SetGame model) throws IOException {
		this.model = model;
		this.port = 10501;
		this.gameController = new GameController(this.model);
	}
	/**
	 * Run method, listens for new clients attempting to connect and pairs them with newly
	 * instantiated playerHandlers
	 */
	public void run() {
		ServerSocket ss = null;
		try {
			gameController.message("Server started at port " + port);
			boolean listening  = true;
			try {
				ss = new ServerSocket(port);
				System.out.println("Listening");
				int playerNumber = 1;
				while (listening) {
					new PlayerHandler(ss.accept(),this.gameController,playerNumber).start();
					playerNumber++;
					System.out.println("accepted, starting connection");
				}
			}catch(Throwable t) {
				throw new Error("Bad server: " + t);
			}
			finally {
				ss.close(); //try this, remove if it doesn't work.
			}
		}catch(Exception e){
			System.out.println("Remote connection reset");
		}
	}

	
}
