package lab5.view;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Wrapper of DataOutputStream, contains new protocol 
 * for server-client messages.
 * @author David Young (dcyoung@go.wustl.edu)
 *		File: SetMsgOutputStream.java
 *
 */
public class SetMsgOutputStream extends DataOutputStream {
	/**
	 * Constructor
	 * @param out
	 */
	public SetMsgOutputStream(OutputStream out) {
		super(out);
	}

	/**
	 * Preliminary message to notify client that a message will be sent.
	 * Sends a message delimiter which the client can recognize.
	 * @param type The type of message being sent
	 * @param length the length of the message being sent
	 * @throws IOException
	 */
	public void startMsg(int type, int length) throws IOException {
		this.writeByte('!');		//message delimiter
		this.writeByte(type);		//message type
		this.writeShort(length);	//int length (bytes)
	}
	
	/**
	 * Sends cards attributes of cards on field to client
	 * @param cardAttributesSendable
	 * @throws IOException
	 */
	public void sendCardAttributes(int[][] cardAttributesSendable) throws IOException{
		startMsg(77,36*4);
		for (int i = 0;i<12;i++){
			for (int j=0; j<4; j++){
				this.writeInt(cardAttributesSendable[i][j]);
			}
		}
	}
	
	/**
	 * if the checkSet was valid
	 * @throws IOException
	 */
	public void sendValidSet() throws IOException {
		
	}
	
	/**
	 * if the checkSet was invalid
	 * @throws IOException
	 */
	public void sendInvalidSet() throws IOException {
		
	}
	
	/**
	 * send update to client regarding who is playing and their score, so that the gui can change the people in game appropriately
	 * @param playerIDs
	 * @param playerScores
	 * @param playerNames
	 * @throws IOException
	 */
	public void statsUpdate(int[] playerIDs, int[] playerScores, String[] playerNames) throws IOException {
		int numPlayers = playerIDs.length;
		int length= numPlayers*3;
		startMsg(8,length);
		this.writeInt(numPlayers);
		for(int i = 0; i < numPlayers; i++) {
			//send all as strings so that the gui updates easily
			this.writeUTF("" + playerIDs[i]);
			this.writeUTF("" + playerNames[i]);
			this.writeUTF("" + playerScores[i]);
		}
	}
	
	
	/**
	 * if the game is over, notify the client, and tell them who won.
	 */
	public void gameOver() throws IOException {
		startMsg(60, 0);
	}

	/**
	 * Send Integer
	 */
	public void sendInt (int type, int length, int toSend) throws IOException{
		startMsg(type, length);
		this.writeInt(toSend);
	}

	/**
	 * Send String
	 */
	public void sendUTF (int type, int length, String toSend) throws IOException{
		startMsg(type, length);
		this.writeUTF(toSend);
	}

	/**
	 * Send Bytes
	 */
	public void sendBytes(int type, int length, String toSend) throws IOException{
		startMsg(type, length);
		this.writeBytes(toSend);
	}

	/**
	 * Send Byte
	 */
	public void sendByte (int type, int length, int toSend) throws IOException{
		startMsg(type, length);
		this.writeByte(toSend);
	}

}
