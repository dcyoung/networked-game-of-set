package lab5.view;

import java.io.DataInputStream;
import java.io.InputStream;
/**
 * Wrapper class for DataInputStream.
 * @author David Young (dcyoung@go.wustl.edu)
 *		File: SetMsgInputStream.java
 *
 */
public class SetMsgInputStream extends DataInputStream {
	public SetMsgInputStream(InputStream in) {
		super(in);
	}

	
}
