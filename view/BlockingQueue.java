package lab5.view;

import java.util.LinkedList;

/**
 * @param <T>
 *            the type of object placed on the queue For the purposes of this game,
 *            it will be of type Runnable
 * @author David (dcyoung@go.wustl.edu)
 * 		 File: BlockingQueue.java
 */
public class BlockingQueue<T> {
	private boolean isQueued;
	private LinkedList<T> queue;
	private int maxSize;
	int count;

	/**
	 * A queue that causes its caller to wait if the queue is empty for dequeue
	 * or the queue is full for enqueue
	 * 
	 * @param maxSize
	 *            -- maximum size of the queue
	 */
	public BlockingQueue(int maxSize) {
		this.maxSize = maxSize;
		queue = new LinkedList<T>();
		isQueued = false;
		count = 0;
	}

	/**
	 * Return the next element from the queue, waiting until the queue is not
	 * empty.
	 * 
	 * @return first element in the queue
	 */
	public synchronized T dequeue() {
		while (queue.isEmpty()) {
			try {
				wait(); // releases lock
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}
		T ans = queue.removeFirst();
		count--;
		notifyAll(); // notifies threads to check again
		return ans;
	}

	/**
	 * Add an element to the queue, blocking until the queue is not full
	 * 
	 * @param The
	 *            object to be enqueued
	 */
	public synchronized void enqueue(T obj) {
		while (!(count < maxSize)) {
			try {
				wait(); // releases lock
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		isQueued = true;
		notifyAll(); // notifies threads to check again
		count++;
		queue.addLast(obj);
	}

	/**
	 * Remove all the runnables if the game is over
	 */
	public void removeAll() {
		this.queue.removeAll(queue);
		System.out.println("Blocking Queue has been flushed");
	}
}