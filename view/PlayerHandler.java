package lab5.view;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 * An instance of this class sits between each Client and the server.
 * 
 * @author David Young (dcyoung@go.wustl.edu)
 * 		 File: PlayerHandler.java
 * 
 */
public class PlayerHandler extends Thread {

	private Socket socket;
	private GameController gameController;
	private int playerNum;
	private String playerName;
	private static int num = 0;
	private boolean shouldDie;
	private DataInputStream in;
	private DataOutputStream out;
	private SetMsgOutputStream output;

	/**
	 * Constructor Creates a PlayerHandler
	 * 
	 * @param s
	 * @param g
	 * @param playerNumber
	 */
	public PlayerHandler(Socket s, GameController g, int playerNumber) {
		this.socket = s;
		this.gameController = g;
		this.playerNum = playerNumber;
		this.shouldDie = false;
		try {
			in = new DataInputStream(socket.getInputStream());
			out = new DataOutputStream(socket.getOutputStream());
			output = new SetMsgOutputStream(out);
		} catch (IOException e) {
			System.out.println("Error Creating a PLayerHandler");
			e.printStackTrace();
		}
	}

	/**
	 * Sleep
	 * 
	 * @param time
	 */
	private void sleep(int time) {
		int snooze = time;
		try {
			Thread.sleep(snooze);
		} catch (Throwable t) {
			throw new Error("bad sleep " + t);
		}

	}

	/**
	 * listens for mesgs from the associated client and responds accordingly
	 */
	public void run() {

		try {
			while (!shouldDie) {
				char c = (char) in.readByte();
				while ((c != '!') && (!shouldDie)) {
					c = (char) in.readByte();
					sleep(500);
				}
				int type = in.readByte();
				int length = in.readShort();
				switch (type) {
				case 0:
					initializeBothSides();
					break;
				case 76:
					clientCallsSet();
					break;
				}
				sleep(1000);
			}
		} catch (Throwable t) {
			System.out.println("Noting exception for " + this);
		} finally {
			gameController.removePlayer(this);
			try {
				socket.close();
				System.out
						.println("Succesfully closed the PlayerHandler Socket");
			} catch (IOException e) {
				System.out
						.println("Error Attempting to close the PlayerHandler Socket");
				e.printStackTrace();
			}
		}

	}

	/**
	 * Sends cards on the field to the client.
	 * 
	 * @param cardAttributesSendable
	 */
	public void updateClientsCardsOnTable(int[][] cardAttributesSendable) {
		try {
			output.sendCardAttributes(cardAttributesSendable);
		} catch (IOException e) {
			// FIXME Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Send Updated GameStats
	 */
	public void sendUpdatedGameStats(int[] playerIDs, int[] playerScores,
			String[] playerNames) {
		try {
			output.statsUpdate(playerIDs, playerScores, playerNames);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * response to sayHello, informs the client of the current state of the game
	 * informs the model to add the new player
	 */
	public void initializeBothSides() {
		System.out
				.println("Loading....Initializing Cards and Player Statistics");
		try {
			this.playerName = in.readUTF();
			gameController.addPlayer(this);
			gameController.prepNewClient();
		} catch (IOException e) {
			System.out.println("Error attempting to initialize new player");
			e.printStackTrace();
		}

	}

	/**
	 * Read in the mesg from the client that called a set
	 */
	public void clientCallsSet() {
		try {
			int number = this.in.readInt();
			int index1 = this.in.readInt();
			int index2 = this.in.readInt();
			int index3 = this.in.readInt();
			System.out.println("checking Set on the indices " + index1 + " "
					+ index2 + " " + index3);
			this.gameController
					.checkSet(this.playerNum, index1, index2, index3);

		} catch (IOException e) {
			System.out
					.println("An Error Occurred attempting to read the incoming set");
			e.printStackTrace();
		}
	}

	/**
	 * Tell Client the game is over
	 */
	public void gameOver() {
		try {
			output.gameOver();
		} catch (IOException e) {
			System.out.println("Error attempting to end the game");
			e.printStackTrace();
		}
	}

	/**
	 * Sentences the playerHandler to die
	 */
	public void die() {
		shouldDie = true;
		System.out.println("Sentenced PlayerHandler To Die");
	}

	/**
	 * Getter for PlayerNum
	 * 
	 * @return
	 */
	public int getPlayerNum() {
		return playerNum;
	}

	/**
	 * getter for playername
	 * 
	 * @return playerName the name of the player
	 */
	public String getPlayerName() {
		return playerName;
	}

	/**
	 * toString
	 * 
	 * @return the player ID associated with this PlayerHandler
	 */
	public String toString() {
		return "Player #" + playerNum;
	}

}
