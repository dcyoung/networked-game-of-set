package lab5.view;

import java.util.HashSet;
import java.util.Set;

import lab5.model.Player;
import lab5.model.SetGame;

/**
 * GameController, handles communication between all PlayerHandlers and game
 * model.
 * 
 * @author David Young (dcyoung@go.wustl.edu)
 * 		 File: GameController.java
 */
public class GameController extends Thread {

	BlockingQueue<Runnable> runqueue; /* thread-safe */
	Set<PlayerHandler> players;
	SetGame gameModel;
	boolean push;

	/**
	 * Constructor
	 * 
	 * @param gameModel
	 *            SetGame model
	 */
	public GameController(SetGame gameModel) {
		runqueue = new BlockingQueue<Runnable>(10);
		players = new HashSet<PlayerHandler>();
		this.gameModel = gameModel;
		this.push = true;
		this.start();
	}

	/**
	 * Add a runnable to the queue
	 * 
	 * @param r
	 */
	public void addRunnable(Runnable r) {
		runqueue.enqueue(r);
	}

	/*
	 * // Even though the following methods are called from multiple threads
	 * concurrently, none of them need synchronization. Each one simply
	 * adds a Runnable to the queue, and since the queue is serviced one
	 * Runnable at a time, the work is not performed in a multi-threaded
	 * manner
	 */
	/**
	 * Add a Player
	 * 
	 * @param h
	 */
	public void addPlayer(final PlayerHandler h) {
		addRunnable(new Runnable() {
			public void run() {
				players.add(h);
				gameModel.addPlayer(h.getPlayerName());
				message("Player joined: " + h);
				for (PlayerHandler p : players) {
					tellClient(p, "New player here! " + h);
				}
			}
		});
	}

	/**
	 * Removes a player from the model and informs all the clients of the change
	 * 
	 * @param h
	 */
	public void removePlayer(final PlayerHandler h) {
		addRunnable(new Runnable() {
			public void run() {
				players.remove(h);
				gameModel.removePlayer(h.getPlayerNum());
				h.die();
				message("Player left: " + h);
				int numPlayers = gameModel.getPlayers().size();
				int[] playerIDs = new int[numPlayers];
				int[] playerScores = new int[numPlayers];
				String[] playerNames = new String[numPlayers];
				int counter = 0;
				for (Player p : gameModel.getPlayers()) {
					playerIDs[counter] = p.getID();
					playerScores[counter] = p.getScore();
					playerNames[counter] = p.getName();
					counter++;
				}
				for (PlayerHandler p : players) {
					p.sendUpdatedGameStats(playerIDs, playerScores, playerNames);
				}

			}
		});
	}

	/**
	 * Shows the current players
	 */
	public void showPlayers() {
		addRunnable(new Runnable() {
			public void run() {
				for (PlayerHandler p : players) {
					System.out.println(p.toString());
				}
			}
		});
	}

	/**
	 * Tell the client a specific message
	 * 
	 * @param p
	 * @param s
	 */
	public void tellClient(final PlayerHandler p, final String s) {
		addRunnable(new Runnable() {
			public void run() {
				// p.tellClient(s);
				System.out.println("Sent message: " + s);
			}
		});
	}

	/**
	 * Shut down the gameController
	 */
	public void shutDown() {
//		addRunnable(new Runnable() {
//			public void run() {
				System.out.println("Attempting to clear BlockingQueue");
				clearBlockingQueue();
				System.out.println("GameController shutting down");
				System.exit(0);
//			}
//		});
	}

	/**
	 * Print a message
	 * 
	 * @param s
	 */
	public void message(final String s) {
		addRunnable(new Runnable() {
			public void run() {
				System.out.println("message: " + s);
			}
		});
	}

	/**
	 * Check a set to see if valid.
	 * 
	 * @param playerNumber
	 *            player ID that called set.
	 * @param index1
	 * @param index2
	 * @param index3
	 */
	public void checkSet(final int playerNumber, final int index1,
			final int index2, final int index3) {
		addRunnable(new Runnable() {
			public void run() {
				if (gameModel.constitutesSet(
						gameModel.getCardsInPlay().get(index1), gameModel
								.getCardsInPlay().get(index2), gameModel
								.getCardsInPlay().get(index3), false)) {
					gameModel.adjustPlayerScore(playerNumber);
					while ((!gameModel.isSetPossible())
							&& (!gameModel.gameOver)) {
						gameModel.resetCardsInPlay();
					}
					int[][] cardAttributesSendable = gameModel
							.getSendableCardAttributes();
					System.out.println("it was a set");

					int numPlayers = gameModel.getPlayers().size();
					int[] playerIDs = new int[numPlayers];
					int[] playerScores = new int[numPlayers];
					String[] playerNames = new String[numPlayers];
					int counter = 0;
					for (Player p : gameModel.getPlayers()) {
						playerIDs[counter] = p.getID();
						playerScores[counter] = p.getScore();
						playerNames[counter] = p.getName();
						counter++;
					}
					for (PlayerHandler ph : players) {
						ph.updateClientsCardsOnTable(cardAttributesSendable);
						ph.sendUpdatedGameStats(playerIDs, playerScores,
								playerNames);
					}
				} else {
					System.out.println("it was not a set");
					// method that writes message back to client that was not a
					// set
				}
				if (gameModel.gameOver) {
//					push = false;
					for (PlayerHandler ph : players) {
						ph.gameOver();
						removePlayer(ph);
					}
					snooze(4);
					shutDown();
				}
			}
		});
	}

	/**
	 * Prepare all the information needed for a new client and inform the client
	 */
	public void prepNewClient() {
		addRunnable(new Runnable() {
			public void run() {
				int[][] cardAttributesSendable = gameModel
						.getSendableCardAttributes();

				int numPlayers = gameModel.getPlayers().size();
				int[] playerIDs = new int[numPlayers];
				int[] playerScores = new int[numPlayers];
				String[] playerNames = new String[numPlayers];
				int counter = 0;
				for (Player p : gameModel.getPlayers()) {
					playerIDs[counter] = p.getID();
					playerScores[counter] = p.getScore();
					playerNames[counter] = p.getName();
					counter++;
				}
				for (PlayerHandler p : players) {
					System.out.println("sending card attributes");
					p.updateClientsCardsOnTable(cardAttributesSendable);
					p.sendUpdatedGameStats(playerIDs, playerScores, playerNames);
				}
			}
		});
	}

	/**
	 * run method, dequeues the runnables in the queue in FIFO order, and then
	 * runs them
	 */
	public void run() {
		while (push) {
			Runnable r = runqueue.dequeue();
			r.run();
		}
	}

	/**
	 * Snooze
	 * 
	 * @param secs
	 */
	private static void snooze(int secs) {
		try {
			Thread.sleep(secs * 1000);
		} catch (Throwable t) {
			throw new Error("Bad sleep: " + t);
		}
	}

	/**
	 * Flush the Blocking Queue
	 */
	private void clearBlockingQueue() {
		this.runqueue.removeAll();
	}

	/**
	 * Main method (Unused)
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		snooze(5);
	}

}
