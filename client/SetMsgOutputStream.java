package lab5.client;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
/**
 * Specific Msg Output Stream for the CLient... compartmentalizes the formation of specific types of 
 * messages sent to the playerHandler so that they may simply be called as methods from the client
 * @author David, Paras, Erin
 *       Emails: dcyoung@wustl.edu, paras.vora@wustl.edu, eswansen@wustl.edu
 * 		 Lab: 5b File: SetMsgOutputStream.java
 *
 */
public class SetMsgOutputStream extends DataOutputStream {
/**
 * Constructor
 * @param out
 */
	public SetMsgOutputStream(OutputStream out) {
		super(out);
		// FIXME Auto-generated constructor stub
	}
	/**
	 * Specific format to begin each mesg with a dlimiter and a mesg type followed by length of the payload
	 * @param type
	 * @param length
	 * @throws IOException
	 */
	public void startMsg(int type, int length) throws IOException {
		this.writeByte('!');		//message delimiter
		this.writeByte(type);		//message type
		this.writeShort(length);	//int length (bytes)
	}

	
	/*
	 * Send Integers
	 */
	public void sendInt (int type, int length, int toSend) throws IOException{
		startMsg(type, length);
		this.writeInt(toSend);
	}
	
	/*
	 * Send the indices of a set
	 */
	public void sendSetIndices(int type, int length, int playerNum, int index1, int index2, int index3) throws IOException{
		startMsg(type,length);
		this.writeInt(playerNum);
		this.writeInt(index1);
		this.writeInt(index2);
		this.writeInt(index3);
		
	}
	
	/*
	 * Send Strings
	 */
	public void sendUTF (int type, int length, String toSend) throws IOException{
		startMsg(type, length);
		this.writeUTF(toSend);
	}

	/*
	 * Send Bytes
	 */
	public void sendBytes(int type, int length, String toSend) throws IOException{
		startMsg(type, length);
		this.writeBytes(toSend);
	}

	/*
	 * Send Bytes
	 */
	public void sendByte (int type, int length, int toSend) throws IOException{
		startMsg(type, length);
		this.writeByte(toSend);
	}
	
}
