package lab5.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JOptionPane;

import lab5.client.SetMsgInputStream;
import lab5.client.SetMsgOutputStream;
import lab5.guiFramework.GameGUI;
import lab5.guiFramework.Window;


/**
 *  Client
 *  TO test the game, run the ServerMain.java from the lab5 package... 
 *	then run a Client.java from the lab5.client package, read its instructions 
 *	and click through its menu.  BE SURE to get through the main menu before opening a second client!!!  
 * @authors David Young
 */

/**
 * Client, handles all of the reading and interpreting of the input from the connection over the network,
 * and tells the gui to make changes... 
 *       Emails: dcyoung@wustl.edu
 *       File Name: Client.java
 */
public class Client {
	private Socket soc;
	private DataInputStream dataInStrm;
	private DataOutputStream dataOutStrm;
	private SetMsgInputStream input;
	private SetMsgOutputStream output;
	public int playerNumber;
	public String playerName = "";
	public int playerScore;
	private Window window;
	private GameGUI associatedGUI;
	public boolean gameStarted;
	boolean loop;
	
	/**
	 * Creates a Client to handle one side of the game connection
	 * Constructor: Must hardcode the IP address of the socket where Main.java is run
	 */
	public Client() throws IOException {
		this.playerName = JOptionPane.showInputDialog("Input your name: ");
		this.playerScore = 0;
		this.playerNumber = 0;
		soc = new Socket("localhost", 10501);
		dataInStrm = new DataInputStream(soc.getInputStream());
		dataOutStrm = new DataOutputStream(soc.getOutputStream());
		input = new SetMsgInputStream(dataInStrm);
		output = new SetMsgOutputStream(dataOutStrm);
		gameStarted = false;
		loop = true;
	}
	
	/**
	 * Set our instance variable associated GUI to the GUI that was formed when the window was run
	 */
	private void setAssociatedGUI(){
		this.associatedGUI = window.getFramework().getGameGUI(); 
	}
	/**
	 * @return the GUI associated with the client
	 */
	private GameGUI getAssociatedGUI(){
		return window.getFramework().getGameGUI();
	}

	/**
	 * Say Hello to instantiate a connection
	 * @throws IOException
	 */
	private void sayHello() throws IOException{
		output.sendUTF(0,1,playerName);
	}
	
	/**
	 * When a player selects 3 cards in the gui and then hits the callSet button, the three indices of the selected Cards are
	 * sent in this message to the server.  The server hands them off to the model "GameOfSet" with its constitutesSet method 
	 * to check if the selection was valid.... 
	 * @param cardIndex1
	 * @param cardIndex2
	 * @param cardIndex3
	 */
	public void callSet(){
		int[] tempIndices = this.getAssociatedGUI().getSelectedIndices();
		try {
			this.output.sendSetIndices(76, 4, this.playerNumber, tempIndices[0], tempIndices[1], tempIndices[2] );
		} catch (IOException e) {
			System.out.println("An Error Occurred attempting to call a Set");
			e.printStackTrace();
		}
	}
	
	/**
	 * Sleep
	 * @param time
	 */
	private void sleep(int time) {
		int snooze = time;
		try {
			Thread.sleep(snooze);
		}
		catch (Throwable t) {
			throw new Error("bad sleep " + t);
		}

	}
	
	/**
	 * Resets the GUI's list of card attributes
	 * @throws IOException
	 */
	private void resetCardAttributes() throws IOException{
		this.getAssociatedGUI().setCardsChanging(true);
		int[][] tempCardAttributes = new int[12][4];
		for (int i = 0;i<12;i++){
			for (int j=0; j<4; j++){
				int temp = this.input.readInt();
				tempCardAttributes[i][j] = temp;
			}
		}
		this.getAssociatedGUI().setCardAttributes(tempCardAttributes);
		this.getAssociatedGUI().clearSelectionMarkers();
		this.sleep(1250);// change this to vary length msg is shown that cards are changing
		this.getAssociatedGUI().setCardsChanging(false);
	}
	/**
	 * Update the GUI's information about each player
	 * @param length
	 * @throws IOException
	 */
	private void playerInfoUpdate(int length) throws IOException{
		int numPlayers = input.readInt();
		this.getAssociatedGUI().clearAllPlayerInfo();
		for(int i =0; i<numPlayers; i++){
			String s1 = input.readUTF();
			String s2 = input.readUTF();
			String s3 = input.readUTF();
			this.getAssociatedGUI().addPlayerInfo(s1,s2,s3);
		}
	}
	
	/**
	 * End the Game
	 */
	private void endGame(){
		this.loop = false;
		this.window.getFramework().endGame();
		sleep(2000);
	}
	
	/**
	 * Instantiates the window and all of the gui by extension.  
	 * Declares itself to the server and continually interprets 
	 * incoming messages from the server, responding accordingly. 
	 */
	private void run() {
		window = new Window(this);
		this.setAssociatedGUI();
		try {
			while(this.gameStarted==false){
				sleep(5000);
			}
			this.sayHello();
			while(loop){
				char c = (char)input.readByte();
				while (c != '!') {
					c = (char)input.readByte();
					sleep(500);
				}
				int type = input.readByte();
				int length = input.readShort();
				switch (type){
					case 77: resetCardAttributes();
						break;
					case 8:  playerInfoUpdate(length);
						break;
					case 60: endGame();
						break;
				}
				sleep(1000);
			}
		} catch (Exception e){
			System.out.println("An error occurred attempting to run Client");
			e.printStackTrace();
		}
		finally{
			try {
				this.soc.close();
				System.out.println("Succesfully Closed the Client Socket");
			} catch (IOException e) {
				System.out.println("Error CLosing the Client Socket");
				e.printStackTrace();
			}
		}
	}
	
	
	/**
	 * Kicks off the client
	 * @param args unused
	 */
	public static void main(String[] args) {
		Client c;
		try {
			c = new Client();
			System.out.println("Created Client");
			c.run();
		} catch (IOException e) {
			System.out.println("Client Creation Failed");
			e.printStackTrace();
		}
		
		
	}
}
