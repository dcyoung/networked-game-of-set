package lab5.guiFramework;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

/**
 * @author David Young (dcyoung@wustl.edu)
 * The card class.  This holds all the information about the drawable card objects for the GUI...
 * these are not the actual card objects held in the model... they are just representations for the GUI.
 * 		 File: Card.java
 */

public class Card {
    
    /**
     * How much time must pass in order to create a new card?
     */
    public static long timeBetweenCards = Framework.secInNanosec / 2;
    /**
     * Last time when the Card was created.
     */
    public static long lastCardTime = 0;
    
    /**
     * Card lines.
     * Where is starting location for the card?
     * How many points is a card worth?
     */
    public static int[][] cardSlot = {
    	{(int)(Framework.frameWidth*2/3*1/4), (int)(Framework.frameHeight * 1/5), 20},
    	{(int)(Framework.frameWidth*2/3*1/4), (int)(Framework.frameHeight * 2/5), 20},
    	{(int)(Framework.frameWidth*2/3*1/4), (int)(Framework.frameHeight * 3/5), 20},
    	{(int)(Framework.frameWidth*2/3*1/4), (int)(Framework.frameHeight * 4/5), 20},
        {(int)(Framework.frameWidth*2/3*2/4), (int)(Framework.frameHeight * 1/5), 30},
        {(int)(Framework.frameWidth*2/3*2/4), (int)(Framework.frameHeight * 2/5), 30},
        {(int)(Framework.frameWidth*2/3*2/4), (int)(Framework.frameHeight * 3/5), 30},
        {(int)(Framework.frameWidth*2/3*2/4), (int)(Framework.frameHeight * 4/5), 30},
        {(int)(Framework.frameWidth*2/3*3/4), (int)(Framework.frameHeight * 1/5), 40},
        {(int)(Framework.frameWidth*2/3*3/4), (int)(Framework.frameHeight * 2/5), 40},
        {(int)(Framework.frameWidth*2/3*3/4), (int)(Framework.frameHeight * 3/5), 40},
        {(int)(Framework.frameWidth*2/3*3/4), (int)(Framework.frameHeight * 4/5), 40},
                                      };
    /**
     * Indicate which is next card line.
     */
    public static int nextCardSlot = 0;
    
    
    /**
     * X coordinate of the Card.
     */
    public int x;
    /**
     * Y coordinate of the card.
     */
    public int y;
    
    /**
     * How many points this card is worth?
     */
    public int score;
    
    /**
     * Card image.
     */
    private BufferedImage cardImg;
	private int cardImgHeight;
	private int cardImgWidth;
    private double scaleFactor;
    
	/**
     * Card image Highlight 
     */
	private BufferedImage cardOutlineImg;
    
    /**
     * Creates new card.
     * 
     * @param x Starting x coordinate.
     * @param y Starting y coordinate.
     * @param score How many points this card is worth?
     * @param cardImg Image of the card.
     * @param cardOutlineImg Image of the card highlight outline
     */
    public Card(int x, int y, int score, BufferedImage cardImg, BufferedImage cardOutlineImg)
    {
        this.x = x;
        this.y = y;
        
        this.score = score;
        
        this.cardImg = cardImg;
        this.cardImgWidth = this.cardImg.getWidth();
        this.cardImgHeight = this.cardImg.getHeight();
//        scaleFactor = 0.25;
        scaleFactor = 1;
        this.cardOutlineImg = cardOutlineImg;
        
    }
    
    
    /**
     * Move the card.
     */
    public void Update()
    {
//        x += speed;
    }
    
    /**
     * Draw the card to the screen.
     * @param g2d Graphics2D
     */
    public void Draw(Graphics2D g2d, boolean isSelected)
    {
    	if(isSelected){
    		g2d.drawImage(cardImg, (int)(x-cardImgWidth*scaleFactor/2), (int)(y-cardImgHeight*scaleFactor/2), (int)(cardImgWidth*scaleFactor),(int)(cardImgHeight*scaleFactor), null);
    		g2d.drawImage(cardOutlineImg, (int)(x-cardImgWidth*scaleFactor/2), (int)(y-cardImgHeight*scaleFactor/2), (int)(cardImgWidth*scaleFactor),(int)(cardImgHeight*scaleFactor), null);
    	}else{
    		g2d.drawImage(cardImg, (int)(x-cardImgWidth*scaleFactor/2), (int)(y-cardImgHeight*scaleFactor/2), (int)(cardImgWidth*scaleFactor),(int)(cardImgHeight*scaleFactor), null);
    	}
    }
    /**
     * Draw the card Highlight (representing selection) to the screen
     * @param g2d Graphics 2D
     */
    public void drawSelectionOutline(Graphics2D g2d){
    	g2d.drawImage(cardOutlineImg, (int)(x-cardImgWidth*scaleFactor/2), (int)(y-cardImgHeight*scaleFactor/2), (int)(cardImgWidth*scaleFactor),(int)(cardImgHeight*scaleFactor), null);
    	
    }
    /**
     * Getter for the card image height
     * @return
     */
    public int getCardImgHeight() {
    	return cardImgHeight;
    }
    
    /**
     * Getter for the card image width
     * @return
     */
    public int getCardImgWidth() {
    	return cardImgWidth;
    }
    /**
     * Getter for the card image scale Factor
     * @return
     */
    public double getScaleFactor() {
    	return scaleFactor;
    }
}
