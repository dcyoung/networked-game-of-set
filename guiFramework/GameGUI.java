package lab5.guiFramework;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import lab5.client.Client;

/**
 * @author David Young (dcyoung@wustl.edu)
 * Actual GUI, handles all the dynamics of the actual GUI, including drawing the images, calculating the image files
 * to be drawn, handling selection of cards and displaying of player info etc.
 * 		 File: GameGUI.java
 */

public class GameGUI {
    
	/**
     * client
     */
	private Client client;
	
    /**
     * Font that we will use to write statistic to the screen.
     */
    private Font font;
    
    /**
     * Array list of the cards.
     */
    private ArrayList<Card> cards;
    
    /**
     * Array list of the selected cards.
     */
    Boolean[] selectedCards = new Boolean[12];   
    
    /**
     * counter for the number of Selected Cards
     */
    private int numOfSelectedCards;
    /**
     * set of ints representing the indices of the selected cards
     */
    private HashSet<Integer> selectedIndices;
    
    /**
     * For each killed card, the player gets points.
     */
    private int score;
    
    /**
     * Last time of the click.
     */
    private long lastTimeClick;  
    
    /**
     * The time which must elapse between shots.
     */
    private long timeBetweenClicks;

    /**
     * Game background image.
     */
    private BufferedImage backgroundImg;
    
    /**
     * Card image.
     */
    private BufferedImage blankCardImg;
    
    /**
     * Card Selected Highlighter Image
     */
    private BufferedImage cardOutlineImg;
    
    /**
     * Image to Flash when the cards are refreshing
     */
    private BufferedImage cardsHaveChangedImg;
    
    /**
     * Boolean to tell the draw methods if they should show the changing mesg.
     */
    private boolean cardsChanging;
    
    /**
     * call Set Button Image
     */
    private BufferedImage callSetButtonImg;
    private int callSetButtonXCoordinate = 450;
    private int callSetButtonYCoordinate = 700;
    private double buttonScaleFactor = 0.25;
    
    /**
     * a version of the button to paint when we click on the button
     */
    private BufferedImage callSetButtonPressedImg;
    private boolean callSetButtonPressed;
    private int buttonPressTime; //counter to display the altered button for a few frames
    /**
     * Mouse Pointer image.
     */
    private BufferedImage pointerImg;
    
    /**
     * Middle width of the sight image.
     */
    private int sightImgMiddleWidth;
    
    /**
     * Middle height of the sight image.
     */
    private int sightImgMiddleHeight;

	private boolean withinTestTime = true; 
	
	/**
	 * The array cardAttributes has 12 rows (1 for each card Index) and 4 columns (1 for each card attribute)
	 * column 1 is color, column 2 is color, column 3 is shape, column 4 is shade
	 * int 1 represents number 1, color red, shape diamond and shade hollow
	 * int 2 represents number 2, color green, shape ovals and shade shaded
	 * int 3 represents number 3, color blue, shape squiggle and shade solid
	 */
	private int[][] cardAttributes = new int[12][4];
	
	
	
	/**
	 * Player Info (stored as strings to be drawn on the GUI
	 * for each player there is a vector
	 * each vector holds a vector of player number, name and score stored as strings
	 */
	private Vector<Vector<String>> allPlayerInfo;
	
    
	/**
	 * Constructor
	 */
    public GameGUI(Client c)
    {
    	client = c;
        Framework.guiState = Framework.GuiState.GUI_CONTENT_LOADING;
        
        Thread threadForInitGUI = new Thread() {
            @Override
            public void run(){
                // Sets variables and objects for the game.
                Initialize();
                // Load game files (images, sounds, ...)
                InitialLoadContent();
                
                Framework.guiState = Framework.GuiState.PLAYING;
            }
        };
        threadForInitGUI.start();
    }
    
   /**
     * Set variables and objects for the GUI
     */
    private void Initialize()
    {
    	
        font = new Font("monospaced", Font.BOLD, 18);
        
        cards = new ArrayList<Card>();
        
        Arrays.fill(selectedCards,Boolean.FALSE);
        
        score = 0;
        lastTimeClick = 0;
        timeBetweenClicks = Framework.secInNanosec / 3;
        
        this.allPlayerInfo = new Vector<Vector<String>>();
        addPlayerInfo(""+this.client.playerNumber, this.client.playerName, ""+this.client.playerScore);
        
        this.cardsChanging = false;
        this.numOfSelectedCards = 0;
        this.selectedIndices = new HashSet<Integer>();
        
        client.gameStarted = true;  //set the game started boolean in the client to true
    }
    
    /**
     * Load GUI files - images, sounds, ...
     */
    private void InitialLoadContent()
    {
    	try
    	{
    		URL backgroundImgUrl = this.getClass().getResource("/lab5/resources/images/background/green_swirl_background.jpg");
    		backgroundImg = ImageIO.read(backgroundImgUrl);
    		
    		URL cardImgUrl = this.getClass().getResource("/lab5/resources/images/cards/ace_of_spades.jpg");
    		blankCardImg = ImageIO.read(cardImgUrl);
    		
    		URL cardHighlightImgUrl = this.getClass().getResource("/lab5/resources/images/card_selected_outline.png");
    		cardOutlineImg = ImageIO.read(cardHighlightImgUrl);
    		
    		URL callSetButtonImgUrl = this.getClass().getResource("/lab5/resources/images/button_push.png");
    		callSetButtonImg = ImageIO.read(callSetButtonImgUrl);
    		
    		URL cardsHaveChangedImgUrl = this.getClass().getResource("/lab5/resources/images/cards_have_changed.png");
    		cardsHaveChangedImg = ImageIO.read(cardsHaveChangedImgUrl);
    		
    		URL callSetButtonPressedImgUrl = this.getClass().getResource("/lab5/resources/images/green_button.png");
    		callSetButtonPressedImg = ImageIO.read(callSetButtonPressedImgUrl);
    		callSetButtonPressed = false;
    		buttonPressTime = 1;
    		
    		URL pointerImgUrl = this.getClass().getResource("/lab5/resources/images/pointer.png");
    		pointerImg = ImageIO.read(pointerImgUrl);
    		sightImgMiddleWidth = pointerImg.getWidth() / 2;
    		sightImgMiddleHeight = pointerImg.getHeight() / 2;
    	}
    	catch (IOException ex) {
    		Logger.getLogger(GameGUI.class.getName()).log(Level.SEVERE, null, ex);
    	}
    }
    
    /**
     * Load GUI Card Image Files
     */
    private BufferedImage calculateCardImage(int index){
    	try{
    		URL cardImgUrl = this.getClass().getResource(calculateCardImageURL(index));
    		return ImageIO.read(cardImgUrl);  
    		
    	}catch (IOException ex) {
    		Logger.getLogger(GameGUI.class.getName()).log(Level.SEVERE, null, ex);
    	}
    	return null;
    }
    
    /**
     * Calculate the URL of the Image
     */
    private String calculateCardImageURL(int index){
    	String number, color, shape, shade;
    	int num  = cardAttributes[index][0];
    	int colr = cardAttributes[index][1];
    	int shp  = cardAttributes[index][2];
    	int shd  = cardAttributes[index][3];
    	
    	if(num== 0 || colr== 0 || shp== 0|| shd==0){
    		return "/lab5/resources/card_images/" + "actually_blank_card.png"; 
    	}
    	//Determine number
    	number = "" + num;
    	
    	//Determine color
    	if(colr == 1)
    		color = "Red";
    	else if (colr==2)
    		color = "Green";
    	else 
    		color = "Blue";
    	
    	//Determine shape
    	if(shp == 1)
    		shape = "Diamonds";
    	else if (shp==2)
    		shape = "Ovals";
    	else 
    		shape = "Squiggles";
    	
    	//Determine shade
    	if(shd == 1)
    		shade = "Open";
    	else if (shd==2)
    		shade = "Shaded";
    	else 
    		shade = "Filled";
    	
    	String cardImageURL = "/lab5/resources/card_images/" + number + " " + color + " " + shade + " " + shape + " (3).jpg";		
    	return cardImageURL;
    }
    /**
     * Add a players info to allPlayerInfo
     */
    public void addPlayerInfo(String playerNumber, String playerName, String playerScore){
    	Vector<String> playerInfo = new Vector<String>();
    	playerInfo.addElement(playerNumber); 
    	playerInfo.addElement(playerName);
    	playerInfo.addElement(playerScore);
    	this.allPlayerInfo.addElement(playerInfo);
    }
    /**
     * Clears the allPlayerInfo vector
     */
    public void clearAllPlayerInfo(){
    	this.allPlayerInfo.clear();
    }
    
    /**
     * Setter for cardsChanging
     */
    public void setCardsChanging(boolean status){
    	this.cardsChanging = status;
    }
    /**
     * Setter for cardAttributes
     */
    public void setCardAttributes(int[][] newAttributes){
    	
    	for (int i = 0;i<12;i++){
    		for (int j=0; j<4; j++){
    			this.cardAttributes[i][j] = newAttributes[i][j];
    		}
    	}
    }
    
    
    /**
     * Restart game - reset some variables.
     */
    public void RestartGame()
    {
        // Removes all of the cards from this list.
        cards.clear();
        
        // We set last card time to zero.
        Card.lastCardTime = 0;
        
        score = 0;
        lastTimeClick = 0;
    }
    
    
    /**
     * Update GUI logic.
     * 
     * @param gameTime gameTime of the game.
     * @param mousePosition current mouse position.
     */
    public void UpdateGUI(long gameTime, Point mousePosition)
    {
		// Creates a new card, if it's the time, and add it to the array list.
	    	cards.clear();
    		for(int i=0; i<12;i++)
	        {
	            // Here we create new card and add it to the array list.
//	        	cards.add(new Card(Card.cardSlot[Card.nextCardSlot][0], Card.cardSlot[Card.nextCardSlot][1], Card.cardSlot[Card.nextCardSlot][2], cardImgs,cardOutlineImg));
	    		cards.add(new Card(Card.cardSlot[Card.nextCardSlot][0], Card.cardSlot[Card.nextCardSlot][1], Card.cardSlot[Card.nextCardSlot][2], calculateCardImage(i),cardOutlineImg));
	            // Here we increase nextCardLines so that next card will be created in next line.
	            Card.nextCardSlot++;
	            if(Card.nextCardSlot >= Card.cardSlot.length)
	                Card.nextCardSlot = 0;
	            
	            Card.lastCardTime = System.nanoTime();
	            
	        }
        // Update all of the cards.
        for(int i = 0; i < cards.size(); i++)
        {
            // Move the card.
            cards.get(i).Update();
            
            // Checks if the cards are a set... then deal with that 
            if(cards.get(i).x < 0 - blankCardImg.getWidth())
            {
                cards.remove(i);
            }
        }
        
        // Does player press the mouse?
        if(Canvas.mouseButtonState(MouseEvent.BUTTON1))
        {
            // Checks if it can click again.
            if(System.nanoTime() - lastTimeClick >= timeBetweenClicks)
            {
                //Check to see if the callSetButton was pressed
                if(new Rectangle(callSetButtonXCoordinate, callSetButtonYCoordinate, (int) (callSetButtonImg.getWidth()*buttonScaleFactor), (int) (callSetButtonImg.getHeight()*buttonScaleFactor)).contains(mousePosition)){
                	if(this.selectedIndices.size()==3){
                		callSetButtonPressed = true;
                		client.callSet();
                	}
                }
                
                // We go over all the cards and we look if any of them were selected.
                for(int i = 0; i < cards.size(); i++)
                {
                    // We check, if the mouse was over card when player presses the mouse.
                	if(new Rectangle(cards.get(i).x-(int)(cards.get(i).getCardImgWidth()*cards.get(i).getScaleFactor()/2), cards.get(i).y-(int)(cards.get(i).getCardImgHeight()*cards.get(i).getScaleFactor()/2) , (int)(cards.get(i).getCardImgWidth()*cards.get(i).getScaleFactor()), (int)(cards.get(i).getCardImgHeight()*cards.get(i).getScaleFactor())).contains(mousePosition))
                    {
                        score += cards.get(i).score;
                        
                        // Remove the card from the array list.
//                        cards.remove(i);
                        //Add Cards to Selected List  
                        if (selectedCards[i] == true){
                        		selectedCards[i] = false;
                        		numOfSelectedCards--;
                        		selectedIndices.remove(i);
                        }else{
                        	if(numOfSelectedCards<3){
                        		selectedCards[i] = true;
                        		numOfSelectedCards++;
                        		selectedIndices.add(i);
                        	}
                        }
                        // We found the card that player selected so we can leave the for loop.
                        break;
                    }
                }
                
                lastTimeClick = System.nanoTime();
            }
        }
        
    }
    
    /**
     * Draw the game to the screen.
     * @param g2d Graphics2D
     * @param mousePosition current mouse position.
     */
    public void Draw(Graphics2D g2d, Point mousePosition)
    {
        g2d.drawImage(backgroundImg, 0, 0, Framework.frameWidth, Framework.frameHeight, null);
        
        if(!callSetButtonPressed){
        	g2d.drawImage(callSetButtonImg, callSetButtonXCoordinate, callSetButtonYCoordinate, (int)(callSetButtonImg.getWidth()*buttonScaleFactor), (int)(callSetButtonImg.getHeight()*buttonScaleFactor), null);
        }
        else{
        	g2d.drawImage(callSetButtonPressedImg, callSetButtonXCoordinate, callSetButtonYCoordinate, (int)(callSetButtonImg.getWidth()*buttonScaleFactor), (int)(callSetButtonImg.getHeight()*buttonScaleFactor), null);
        	if(buttonPressTime%4==0){
        		callSetButtonPressed = false;
        	}
        	buttonPressTime++;
        }
        
        
        // Here we draw all the cards.
        for(int i = 0; i < cards.size(); i++)
        {
            if(selectedCards[i]==true){ // Highlight the card if it is   //added this DCYOUNG
            	cards.get(i).Draw(g2d,true);
            }
            else{
            	cards.get(i).Draw(g2d,false); //there seems to be an array indexoutofboundsexception...
            }
            	
        }
    
        g2d.drawImage(pointerImg, mousePosition.x - sightImgMiddleWidth, mousePosition.y - sightImgMiddleHeight, null);
        
        if(this.cardsChanging){
        	g2d.drawImage(cardsHaveChangedImg, 200, 300, null);
        }
        
        g2d.setFont(font);
//        g2d.setColor(Color.darkGray);
        g2d.setColor(Color.WHITE);
        
        /*
         *Draw the Player Info 
         */
        int drawX=700;
        int drawY = 25;
        g2d.drawString("Player #:  " + " Player Name:   "  + " Score",drawX,drawY);
        
        for(int i = 0; i<this.allPlayerInfo.size(); i++){
        	drawY = drawY+25;
        	g2d.drawString(this.allPlayerInfo.get(i).get(0),drawX,drawY);
        	g2d.drawString(this.allPlayerInfo.get(i).get(1),drawX+150,drawY);
        	g2d.drawString(this.allPlayerInfo.get(i).get(2),drawX+325,drawY);
        }
    }
    
    
    /**
     * Draw the game over screen.
     * 
     * @param g2d Graphics2D
     * @param mousePosition Current mouse position.
     */
    public void DrawGameOver(Graphics2D g2d, Point mousePosition)
    {
    	g2d.setFont(new Font("monospaced", Font.BOLD, 60)); 
        Draw(g2d, mousePosition);
        // The first text is used for shade.
        g2d.setColor(Color.black);
        g2d.drawString("Game Over", Framework.frameWidth / 2 - 150, (int)(Framework.frameHeight * 0.65) + 1);
        g2d.setColor(Color.red);
        g2d.drawString("Game Over", Framework.frameWidth / 2 - 150, (int)(Framework.frameHeight * 0.65));
    }
    
    /**
     * returns the indices of the cards that are currently selected
     * @return
     */
    public int[] getSelectedIndices(){
    	int[] indices = new int[3];
    	Iterator<Integer> it = this.selectedIndices.iterator();
    	for(int i = 0; i<3; i++){
    		indices[i] = it.next();
    	}
    	return indices;
    }
    
    /**
     *	Clears all the current markers selecting cards 
     */
    public void clearSelectionMarkers(){
    	for(int i=0; i<12; i++){
    		selectedCards[i] = false;
    		numOfSelectedCards = 0;
    		selectedIndices.clear();
    	}
    }
}
