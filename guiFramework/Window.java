package lab5.guiFramework;

import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import lab5.client.Client;

/**
 * @author David Young (dcyoung@wustl.edu)
 * Creates a frame and set its properties.
 * 		 File: Window.java
 */
public class Window extends JFrame{
        
	private Client client;
	private Framework frmwrk;
    public Window(Client c)
    {
    		this.client = c;
        // Sets the title for this frame.
        this.setTitle("Game of Set. ID: " + c.playerNumber + " Name: " + c.playerName);
        
        // Sets size of the frame.
        if(false){ // Full screen mode (must be set to true)
            this.setUndecorated(true);// Disables decorations for this frame.
            this.setExtendedState(this.MAXIMIZED_BOTH);// Puts the frame to full screen.
        }else{ // Window mode
            // Size of the frame.
            this.setSize(1200, 800);
            // Puts frame to center of the screen.
            this.setLocationRelativeTo(null);
            // So that frame cannot be resizable by the user.
            this.setResizable(false);
        }
        
        // Exit the application when user close frame.
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        this.frmwrk = new Framework(this.client);
        this.setContentPane(frmwrk);
//        this.setContentPane(new Framework());
        this.setVisible(true);
    }
    
    /**
     * Accessor for framework
     * @param args
     */
    public Framework getFramework(){
    	return this.frmwrk;
    }
    
    /**
     * Main
     * @param args
     */
    public static void main(String[] args)
    {
//        // Use the event dispatch thread to build the UI for thread-safety.
//        SwingUtilities.invokeLater(new Runnable() {
//            @Override
//            public void run() {
//            	Client c;
////				try {
//					c = new Client();
//					new Window();//c);
////				} catch (IOException e) {
////					// FIXME Auto-generated catch block
////					e.printStackTrace();
////				}
//                
//            }
//        });
    }
}
