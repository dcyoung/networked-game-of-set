package lab5.model;

/**
 *  SetGame
 *   Model of the game of set
 *
 *  Author: David Young (dcyoung@wustl.edu)
 * 	File: SetGame.java
 *  
 */

/*
 * Hopefully it will work like this... the cards from cards in Play will be cycled through for their attributes 
 * and a set of integers will be sent to the client who will call draw for the corresponding cards on the 
 * gui
 * 
 */
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import lab5.model.CardObj;
import lab5.model.Player;

public class SetGame {
	private int numOfPlayers;
	private String leader;
	private Vector<Player> players; 		/* set of Players that are part of the game*/
	private Vector<CardObj> drawPile; 		/* set of cards that make up the available playable deck*/
	private Vector<CardObj> discardPile; 	/* set of cards that have been played already*/
	private Vector<CardObj> cardsInPlay; 	/* set of cards that are currently on the table*/
	private CardObj blankCard; 				/* Blank card, with attribute # 5 to signify an empty space in the play field*/
	private int numOfBlankCards;
	public boolean gameOver;

	/**
	 * Initializes a game of set. Populates a deck, shuffles it and deals to the
	 * board.
	 */
	public SetGame() {
		this.players = new Vector<Player>();
		this.drawPile = new Vector<CardObj>();
		this.discardPile = new Vector<CardObj>();
		this.cardsInPlay = new Vector<CardObj>();
		this.blankCard = new CardObj(-1, -1, -1, -1);
		this.numOfBlankCards = 0;
		this.gameOver = false;

		
		initializeDeck();					// populate the deck
		shuffleDrawPile(this.drawPile);		// shuffle the deck
		for (int i = 0; i < 12; i++) {
			dealCard();
		}
		System.out.println("the draw Pile is " + this.drawPile.size()
				+ " after dealing initial 12");
		while (!isSetPossible()) {
			System.out.println("set was not possible");
		}
	}

	/**
	 * Getter for cards in play
	 * @return cards in play
	 */
	public Vector<CardObj> getCardsInPlay() {
		return cardsInPlay;
	}

	/**
	 * Add a new Player to the game
	 * @param name
	 */
	public void addPlayer(String name) {
		numOfPlayers++;
		Player p = new Player(name, numOfPlayers);
		if (players.contains(p)) {
			numOfPlayers--;
			return;
		}
		players.add(p);
		System.out.println("Succesfully added a player: " + name);
	}

	/**
	 * Accessor for set of players in game.
	 * @return set of players in game
	 */
	public synchronized Vector<Player> getPlayers() {
		return players;
	}

	/**
	 * Initialize the deck
	 */
	public void initializeDeck() {
		// create the deck
		for (int color = 0; color < 3; color++) {
			for (int num = 0; num < 3; num++) {
				for (int shape = 0; shape < 3; shape++) {
					for (int shade = 0; shade < 3; shade++) {
						/*
						 * The following commented code can replace the above code to have a deck of only 12 cards.. 
						 * this is useful in demonstrating the end conditions for testing.
						 */
//		for (int color = 0; color < 3; color++) {
//			for (int num = 0; num < 2; num++) {
//				for (int shape = 0; shape < 2; shape++) {
//					for (int shade = 0; shade < 1; shade++) {
						this.drawPile.addElement(new CardObj(color, num, shape,
								shade));
					}
				}
			}
		}
		System.out.println("Initial size of the draw Pile is "
				+ this.drawPile.size());
	}

	/**
	 * Deal a card from the deck takes a new card from the deck and adds it to
	 * the cards in play
	 */
	public void dealCard() {
		CardObj deal;
		deal = (CardObj) this.drawPile.elementAt(0);
		this.drawPile.removeElementAt(0);
		this.cardsInPlay.addElement(deal);
	}

	/**
	 * Deal a card to a specific index
	 */
	public void dealCardToIndex(int index) {
		CardObj deal;
		deal = (CardObj) this.drawPile.elementAt(0);
		this.drawPile.removeElementAt(0);
		this.cardsInPlay.set(index, deal);
	}

	/**
	 * Deal a blank card to a specific index
	 */
	public void dealBlankCardToIndex(int index) {
		this.cardsInPlay.set(index, this.blankCard);
		this.numOfBlankCards++;
	}

	/**
	 * Replace a set
	 */
	public void replaceSet(CardObj a, CardObj b, CardObj c) {
		int[] indices2change = new int[3];
		indices2change[0] = this.cardsInPlay.indexOf(a);
		indices2change[1] = this.cardsInPlay.indexOf(b);
		indices2change[2] = this.cardsInPlay.indexOf(c);
		if (this.drawPile.size() >= 3) {
			for (int i = 0; i < 3; i++) {
				dealCardToIndex(indices2change[i]);
			}
		} else {
			for (int i = 0; i < 3; i++) {
				dealBlankCardToIndex(indices2change[i]);
			}
		}
		System.out.println("Replaced a set and the draw Pile is "
				+ this.drawPile.size());
	}

	/**
	 * Shuffle the deck
	 * @param deck
	 */
	public void shuffleDrawPile(Vector deck) {
		int size = deck.size();
		for (int i = 0; i < size; i++) {
			Object temp;
			int j = (int) (Math.random() * size);
			temp = deck.elementAt(j);
			deck.setElementAt(deck.elementAt(i), j);
			deck.setElementAt(temp, i);
		}
	}

	/**
	 * Re-deals all the cards in play (in the event a set is not possible)
	 */
	public void resetCardsInPlay() {
		for (CardObj c : this.cardsInPlay) {
			this.drawPile.add(c);
			dealCardToIndex(this.cardsInPlay.indexOf(c));
		}
		shuffleDrawPile(this.drawPile);
		System.out
				.println("Resetting the Cards in Play Because no set was possible");
		System.out.println("After Shuffling, the draw pile size is "
				+ this.drawPile.size());
	}

	/**
	 * check to see if 3 selected cards are a set
	 * @param a
	 * @param b
	 * @param c
	 * @return
	 */
	public boolean constitutesSet(CardObj a, CardObj b, CardObj c,
			boolean justChecking) {
		if (a == b || a == c || b == c)
			return false;

		int num = (a.number + b.number + c.number) % 3;
		int shape = (a.shape + b.shape + c.shape) % 3;
		int shade = (a.shade + b.shade + c.shade) % 3;
		int color = (a.color + b.color + c.color) % 3;
		if (num == 0 && shape == 0 && shade == 0 && color == 0) {
			if (justChecking) {
				resetCardsInPlay();
			} else {
				replaceSet(a, b, c);
			}
			return true;

		} else {
			return false;
		}
	}

	/**
	 * Does set exist on the table currently
	 * @return
	 */
	public boolean isSetPossible() {
		for (int i = 0; i < this.cardsInPlay.size() - 2; i++) {
			for (int j = i + 1; j < this.cardsInPlay.size() - 1; j++) {
				for (int k = j + 1; k < this.cardsInPlay.size(); k++) {
					if (constitutesSet((CardObj) this.cardsInPlay.elementAt(i),
							(CardObj) this.cardsInPlay.elementAt(j),
							(CardObj) this.cardsInPlay.elementAt(k), true)) {
						return true;
					}
				}
			}
		}
//		if (this.drawPile.isEmpty() && (this.numOfBlankCards >= 9)) {
		if (this.drawPile.isEmpty()) {
			this.gameOver = true;
			System.out.println("No More Sets Exist, Game is Over");
		}
		return false;
	}

	/**
	 * Can continue to Play? if false, need to deal a card or maybe end the game
	 * @return true if board has a set, 12 cards or if the deck is empty.
	 */
	public boolean continuePlay() {
		if (this.drawPile.size() == 0) {
			return true;
		} else if (cardsInPlay.size() < 12) {
			return false;
		} else {
			return isSetPossible();
		}
	}

	/**
	 * Gets the attributes of every single card
	 * @return
	 */
	public int[][] getSendableCardAttributes() {
		int[][] cardAttributesSendable = new int[12][4];
		for (int i = 0; i < 12; i++) {
			cardAttributesSendable[i][0] = this.cardsInPlay.get(i).number + 1;
			cardAttributesSendable[i][1] = this.cardsInPlay.get(i).color + 1;
			cardAttributesSendable[i][2] = this.cardsInPlay.get(i).shape + 1;
			cardAttributesSendable[i][3] = this.cardsInPlay.get(i).shade + 1;
		}
		return cardAttributesSendable;
	}

	/**
	 * Adjusts the score a specific player
	 * @param playerNumber
	 */
	public void adjustPlayerScore(int playerNumber) {
		for (Player p : players) {
			if (p.getID() == playerNumber) {
				System.out.println("Adjusting players score by 1");
				p.setScore(p.getScore() + 1);
			}
		}
	}

	/**
	 * Removes a player from the model
	 * @param playerNum
	 */
	public void removePlayer(int playerNum) {
		numOfPlayers--;
		for (Player p : players) {
			if (p.getID() == playerNum) {
				players.remove(p);
				System.out.println("Succesfully removed player: " + playerNum);
				return;
			}
		}
	}
}
