package lab5.model;
/**
 * CardObj- holds all the information pertinent to a card in the game of set, including
 * all of its attributes... note the image files used aren't ovals... they're actually rectangles.
 * @author David (dcyoung@wustl.edu)
 * 		 File: CardObj.java
 *
 */
public class CardObj {
	
	int color, number, shade, shape;

	public static final String numberValues [] = {"one", "two", "three"};
	public static final String shapeValues [] = {"diamond", "oval", "squiggle"};
    public static final String colorValues [] = {"red", "green", "blue"};
    public static final String shadeValues [] = {"hollow", "shaded", "solid"};
    public static final String attributeValues [][] = {numberValues, colorValues,
            shapeValues, shadeValues};
    public static final String attributes [] = {"number", "color",
            "shape", "shade"};
    boolean selected;

    /**
     * Constructor
     * @param c
     * @param n
     * @param sp
     * @param sd
     */
    public CardObj(int c, int n, int sp, int sd) {
        color = c;
        number = n;
        shape = sp;
        shade = sd;
        selected = false;
    }
    
    /**
     * returns the number for the attribute 
     * @param s
     * @return
     */
    public int getAttribute (String s) {
        switch (s) {
        case "color":
            return color;
        case "number":
            return number;
        case "shape":
            return shape;
        case "shade":
            return shade;
        default:
            return -1;
        }
    }
    /**
     * Set a card to selected
     * @param s
     */
    public void setSelected (boolean s) {
        if (selected != s) {
            selected = s;
        }
    }
    
    public String colorValue () {
        return colorValues [color];
    }
    public String numberValue () {
        return numberValues [number];
    }
    public String shapeValue () {
        return shapeValues [shape];
    }
    public String shadeValue () {
        return shadeValues [shade];
    }
    /**
     * Returns a cardObj that would complete a set given 2 cards
     * We didn't end up implementing most of the intelligent code we wrote to
     * find and give hints about sets
     * @param a
     * @param b
     * @return
     */
    public static CardObj getMatchingCard (CardObj a, CardObj b) {
        return new CardObj (2 - (a.color + b.color + 2) % 3, 
                            2 - (a.number + b.number + 2) % 3,
                            2 - (a.shape + b.shape + 2) % 3,
                            2 - (a.shade + b.shade + 2) % 3);

    }
    /**
     * To string
     */
    public String toString(){
    	return "" + number + " " + color + " " + shape + " " + shade; //not written congruent with order conventions.
    }
    
}
