package lab5.model;
/**
 * A class Player to hold all the attributes of a player in the game
 * @author David (dcyoung@wustl.edu)
 * 		 File: Player.java
 */
public class Player extends Actor {
	private String name; 		// name of the Player
	private int ID;				// player's ID
	private int score;   		// player's score in game (# of completed Sets)
	private boolean isWinning; 	// is the player currently winning the game
	
	/**
	 * Constructor
	 * @param name name of the Player
	 */
	public Player(String name, int ID) {
		super(name);
		this.name = name;
		this.ID = ID;
		this.score = 0;
		this.isWinning = false;
	}
	
	/**
	 * Accessor for name.
	 * @return name of the Player
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Accessor for score.
	 * @return score of the Player
	 */
	public int getScore() {
		return this.score;
	}
	
	
	/**
	 * Accessor for player's ID.
	 * @return ID
	 */
	public synchronized int getID() {
		return this.ID;
	}
	
	/**
	 * Accessor for isWinning
	 */
	public boolean getIsWinning(){
		return isWinning;
	}
	
	/**
	 * Mutator for score.
	 */
	public void setScore(int score) {
		this.score = score;
		status(""+score);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Player other = (Player) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return getName();
	}

}
