package lab5.model;

abstract public class Actor extends StatusChanger {

	protected final String name;
	
	/**
	 * Did not modify this class at all -David Young
	 * Manage reporting of actions via PCS.
	 * @param name person taking action
	 * @author cytron
	 */
	public Actor(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see weasley.model.StatusChanger#getName()
	 */
	public String getName() {
		return name;
	}
	
}